import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        #self.driver = webdriver.Firefox()
      

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("http://www.python.org")
        self.assertIn("Python", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("pycon")
        elem.send_keys(Keys.RETURN)

        if driver.capabilities['browserName'] ==  "firefox":
            time.sleep(5)
        
        driver.save_screenshot("./screenshots/test4.png")
        assert "No results found." not in driver.page_source

    def test_search_in_google(self):
        driver = self.driver
        driver.get("http://www.google.com")
        self.assertIn("Google", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("selenium")
        elem.send_keys(Keys.RETURN)
        
        if driver.capabilities['browserName'] ==  "firefox":
            time.sleep(5)
        
        driver.save_screenshot("./screenshots/test4-google.png")
        

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()

